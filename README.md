# acme-setup

![](https://img.shields.io/badge/written%20in-bash-blue)

Scripts to generate and automate ACME TLS certificate deployment.

Tags: sysadmin

## Changelog

2015-12-13 1.0.0
- Initial public release
- [⬇️ acme-setup-1.0.0.tar.xz](dist-archive/acme-setup-1.0.0.tar.xz) *(1.69 KiB)*

